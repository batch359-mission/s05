# 1.

from abc import ABC, abstractclassmethod

class Person(ABC):
	def __init__(self, first_name, last_name, email, department):
	    self._first_name = first_name
	    self._last_name = last_name
	    self._email = email
	    self._department = department

	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

# 2. 

class Employee(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__(first_name, last_name, email, department)

	# firstname getter
	def getFirstName(self):
		return self._firstName

	# firstname setter
	def setFirstName(self, firstName):
		self._firstName = firstName

	# lastname getter
	def getLastName(self):
		return self._last_name

	# lastname setter
	def setLastName(self, lastName):
		self._lastName = lastName

	# email getter
	def getEmail(self):
		return self._email

	# email setter
	def setEmail(self, email):
		self._email = email

	# department getter
	def getDepartment(self):
		return self._department

	# department setter
	def setDepartment(self, department):
		self._department = department

	# login
	def login(self):
		return f"{self._email} has logged in"

	# logout
	def logout(self):
		return f"{self._email} has logged out"

	# add request
	def addRequest(self):
		return "Request has been added"

	# checkRequest
	def checkRequest(self):
		pass

	# addUser
	def addUser(self):
		pass

	# getFullName
	def getFullName(self):
		return f"{self._first_name} {self._last_name}"


# 3.

class TeamLead(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__(first_name, last_name, email, department)
		self._members = []

	# firstname getter
	def getFirstName(self):
		return self._firstName

	# firstname setter
	def setFirstName(self, firstName):
		self._firstName = firstName

	# lastname getter
	def getLastName(self):
		return self._lastName

	# lastname setter
	def setLastName(self, lastName):
		self._lastName = lastName

	# email getter
	def getEmail(self):
		return self._email

	# email setter
	def setEmail(self, email):
		self._email = email

	# department getter
	def getDepartment(self):
		return self._department

	# department setter
	def setDepartment(self, department):
		self._department = department

	# members getter
	def get_members(self):
		return self._members

	# members setter
	def setMembers(self, members):
		self._members = members  

	# add member
	def addMember(self, employee):
		self._members.append(employee)

	# login
	def login(self):
		return f"{self._email} has logged in"

	# logout
	def logout(self):
		return f"{self._email} has logged out"

	# add request
	def addRequest(self):
		return "Request has been added"

	# add user
	def addUser(self):
		pass

	# getFullName
	def getFullName(self):
		return f"{self._first_name} {self._last_name}"

# 4. 

class Admin(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__(first_name, last_name, email, department)

	# firstname getter
	def getFirstName(self):
		return self._firstName

	# firstname setter
	def setFirstName(self, firstName):
		self._firstName = firstName

	# lastname getter
	def getLastName(self):
		return self._lastName

	# lastname setter
	def setLastName(self, lastName):
		self._lastName = lastName

	# email getter
	def getEmail(self):
		return self._email

	# email setter
	def setEmail(self, email):
		self._email = email

	# department getter
	def getDepartment(self):
		return self._department

	# department setter
	def setDepartment(self, department):
		self._department = department

	# login
	def login(self):
		return f"{self._email} has logged in"

	# logout
	def logout(self):
		return f"{self._email} has logged out"

	# add user
	def addUser(self):
		return "User has been added"

	# check request
	def check_request(self):
		pass

	# add request
	def add_request(self):
		pass

	# getFullName
	def getFullName(self):
		return f"{self._first_name} {self._last_name}"

# 5.
class Request():
	def __init__(self, name, requester, date_requested):
		self._name = name
		self._requester = requester
		self._date_requested = date_requested
		self._status = "open"

	# name getter
	def getName(self):
		return (f"{self._name}")

	# name setter
	def setName(self, name):
		self._name = name

	# requester getter
	def getRequester(self):
		return (f"{self._requester}")

	# requester setter
	def setRequester(self, requester):
		self._requester = requester

	# date_requested getter
	def getDateRequested(self):
		return (f"{self._date_requested}")

	# date_requested setter
	def setDateRequested(self, date_requested):
		self._date_requested = date_requested

	# status getter
	def getStatus(self):
		return (f"{self._status}")

	# status setter
	def set_status(self, status):
		self._status = status
		
	def updateRequest(self):
		self._status = "updated"
		return f"Request {self._name} has been updated"

	def closeRequest(self):
		self._status = "closed"
		return f"Request {self._name} has been closed"

	def cancelRequest(self):
		self._status = "cancelled"
		return f"Request {self._name} has been cancelled"



# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
 print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
